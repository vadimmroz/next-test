"use client";
import classes from "./posts.module.scss";
import { useQuery } from "react-query";
import getPosts from "@/api/getPosts";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import { IPost } from "@/type/IPost";
import Post from "@/components/post";

const Posts = ({ search = "" }: { search?: string }) => {
  const [page, setPage] = useState(1);
  const path = usePathname().replace("/", "");
  const [posts, setPosts] = useState<Array<IPost>>([]);
  const { data } = useQuery(["data", page], () => getPosts(path, page, search));
  const handleNextPage = () => {
    setPage((prevState) => prevState + 7);
  };
  useEffect(() => {
    if (data?.data) {
      setPosts((prevState) => [...prevState, ...data?.data]);
    }
  }, [data]);

  return (
    <>
      {posts?.map((e) => {
        return <Post post={e} key={e.id} />;
      })}
      <button className={classes.nextButton} onClick={handleNextPage}>
        <p>Загрузить еще</p>
        <img
          alt=""
          src="https://i.ibb.co/LZWvtQF/premium-icon-worldwide-1968275-r.webp"
        />
      </button>
    </>
  );
};

export default Posts;
