import classes from "./preloader.module.scss";

const Preloader = () => {
  return (
    <div className={classes.preloader}>
      <figure />
    </div>
  );
};

export default Preloader;
