import { useEffect, useLayoutEffect, useState } from "react";
import classes from "@/components/header/components/control/control.module.scss";
import classNames from "classnames";

const useTheme = () => {
  const [switcher, setSwitcher] = useState(classes.switcherCircle);
  const [theme, setTheme] = useState("white");

  useLayoutEffect(() => {
    if (localStorage.getItem("app-theme") === "dark") {
      setTheme("dark");
    }
  }, []);

  useLayoutEffect(() => {
    document.documentElement.setAttribute("data-theme", theme);
    localStorage.setItem("app-theme", theme);
  }, [theme]);

  useEffect(() => {
    if (theme === "dark") {
      setSwitcher(
        classNames(classes.switcherCircle, classes.switcherCircleDark),
      );
    }
  }, [theme, setTheme]);
  const SetSwitcher = () => {
    if (switcher === classes.switcherCircle) {
      setSwitcher(
        classNames(classes.switcherCircle, classes.switcherCircleDark),
      );
      setTheme("dark");
    } else {
      setSwitcher(classes.switcherCircle);
      setTheme("white");
    }
  };
  return { switcher, SetSwitcher };
};

export default useTheme;
