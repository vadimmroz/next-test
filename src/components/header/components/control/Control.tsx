"use client";
import classes from "./control.module.scss";
import useTheme from "@/components/header/components/control/hooks/useTheme";
import { useState } from "react";
import ModalPortal from "@/components/modalPortal/ModalPortal";
import Search from "@/components/search";

const Control = () => {
  const [modal, setModal] = useState(false);
  const { switcher, SetSwitcher } = useTheme();
  const handleOpenSearch = () => {
    setModal((prev) => !prev);
  };
  return (
    <div className={classes.control}>
      <div className={classes.switcher}>
        <div className={switcher} onClick={SetSwitcher} />
      </div>
      <button className={classes.search} onClick={handleOpenSearch}>
        <img
          src=" https://cdn-icons-png.flaticon.com/512/149/149852.png"
          alt=""
        />
      </button>
      <ModalPortal selector="modal" show={modal} onClose={handleOpenSearch}>
        <Search onClose={handleOpenSearch} />
      </ModalPortal>
    </div>
  );
};

export default Control;
