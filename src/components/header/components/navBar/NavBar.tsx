import classes from "./navBar.module.scss";
import { links } from "./utils";
import NavLink from "./components/navLink";
const NavBar = () => {
  return (
    <nav className={classes.navBar}>
      {links.map((e, i) => (
        <NavLink key={i} {...e} />
      ))}
    </nav>
  );
};

export default NavBar;
