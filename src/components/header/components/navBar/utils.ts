export const links = [
  { link: "/all", name: "Статті" },
  { link: "/anime", name: "Аніме" },
  { link: "/games", name: "Ігри" },
  { link: "/films", name: "Фільми" },
  { link: "/other", name: "Інше..." },
];
