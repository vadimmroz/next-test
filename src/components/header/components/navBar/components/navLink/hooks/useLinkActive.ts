import { usePathname } from "next/navigation";

const useLinkActive = (link: string) => {
  const path = usePathname();
  return link === path ? "active" : "";
};

export default useLinkActive;
