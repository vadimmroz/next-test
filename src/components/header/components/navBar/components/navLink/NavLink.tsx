"use client";
import Link from "next/link";
import useLinkActive from "./hooks/useLinkActive";
import classes from "./navLink.module.scss";

const NavLink = ({ name, link }: { name: string; link: string }) => {
  return (
    <Link href={link} className={useLinkActive(link) + ` ${classes.navLink}`}>
      {name}
    </Link>
  );
};

export default NavLink;
