import classes from "./header.module.scss";
import NavBar from "./components/navBar";
import Link from "next/link";
import Control from "./components/control";
import Image from "next/image";

const Header = () => {
  return (
    <>
      <header className={classes.header}>
        <Link href="/all">
          <Image src="/assets/logo.webp" alt="logo" width="132" height="132" />
        </Link>
        <Link href="/all">
          <h1>GO-MERE.COM</h1>
        </Link>
        <NavBar />
        <Control />
      </header>
    </>
  );
};

export default Header;
