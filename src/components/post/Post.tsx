import classes from "./post.module.scss";
import { IPost } from "@/type/IPost";
import moment from "moment";
import Link from "next/link";
const Post = ({ post }: { post: IPost }) => {
  return (
    <Link
      href={`/post/${post.id.toString()}`}
      className={classes.post}
      style={{ backgroundImage: `url('${post.prev}')` }}
    >
      <p>{moment(post.date).format("DD.MM.yyyy")}</p>
      <h2>{post.name}</h2>
    </Link>
  );
};

export default Post;
