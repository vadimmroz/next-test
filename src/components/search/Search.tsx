import classes from "./search.module.scss";
type Props = {
  onClose: () => void;
};
const Search = ({ onClose }: Props) => {
  return (
    <div className={classes.search} onClick={onClose}>
      <form></form>
    </div>
  );
};

export default Search;
