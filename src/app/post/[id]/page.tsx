import { IPost } from "@/type/IPost";
import { supabase } from "@/api/client";
import classes from "./post.module.scss";
import Link from "next/link";
import moment from "moment";

type Props = {
  params: { id: string };
};

export default async function Page(props: Props) {
  const searchParams = props.params.id;
  const { data: posts }: { data: IPost[] | null } = await supabase
    .from("posts")
    .select("*")
    .eq("id", searchParams);
  return (
    <>
      {posts?.map((post) => {
        return (
          <main className={classes.post} key={post.id}>
            <div className={classes.post_menu}>
              <h4>{moment(post.date).format("DD.MM.YYYY")}</h4>
              <div>
                {post.tags.map((e, i) => {
                  return (
                    <Link key={i} href={`/search/tag=${e}`}>
                      #{e}
                    </Link>
                  );
                })}
              </div>
            </div>
            <section>
              <h2>{post.name}</h2>
              <img src={post.prev} alt="prev" />
              {post.text.map((e) => {
                if (e.regim === "text") {
                  return <p key={Math.random()}>{e.data}</p>;
                } else if (e.regim === "big") {
                  return (
                    <p key={Math.random()} className={classes.big}>
                      {e.data}
                    </p>
                  );
                } else if (e.regim === "img") {
                  return <img key={Math.random()} src={e.data} alt="" />;
                } else if (e.regim === "youtube") {
                  return <h4 key={Math.random()}>{e.data}</h4>;
                } else {
                  return;
                }
              })}
            </section>
          </main>
        );
      })}
    </>
  );
}
