import { createClient } from "@/utils/supabase/server";
import { IPost } from "@/type/IPost";
import Post from "@/components/post";

export default async function All() {
  const supabase = createClient();
  const { data: posts }: { data: IPost[] | null } = await supabase
    .from("posts")
    .select("*")
    .like("type", "anime")
    .limit(7)
    .order("date", { ascending: false });

  return (
    <main className="posts">
      {posts?.map((e) => {
        return <Post post={e} key={e.id} />;
      })}
    </main>
  );
}
