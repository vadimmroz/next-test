import { createClient } from "@/utils/supabase/server";
import { IPost } from "@/type/IPost";
import Post from "@/components/post";
import Posts from "@/components/posts";

type Props = {
  params: { slug: string };
};

const SearchByName = async (props: Props) => {
  const name = decodeURI(props.params.slug);
  const supabase = createClient();
  const { data: posts }: { data: IPost[] | null } = await supabase
    .from("posts")
    .select("*")
    .ilike("name", `%${name}%`)
    .limit(7);
  return (
    <main className="posts">
      {posts?.map((e) => {
        return <Post post={e} key={e.id} />;
      })}
      <Posts search={name} />
    </main>
  );
};

export default SearchByName;
