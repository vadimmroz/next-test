import type { Metadata } from "next";
import "./globals.css";
import Header from "@/components/header";
import { ReactNode } from "react";

export const metadata: Metadata = {
  title: "GO-MERE",
  description: "Application for geek from geek",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: ReactNode;
}>) {
  return (
    <html lang="en">
      <body>
        <Header />
        {children}
        <div id="modal" />
      </body>
    </html>
  );
}
