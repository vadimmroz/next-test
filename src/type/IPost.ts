export type Text = {
  regim: "text" | "big" | "img" | "youtube";
  data: string;
};
export interface IPost {
  date: string;
  id: number;
  name: string;
  prev: string;
  tags: string[];
  text: Text[];
  type: string;
}
