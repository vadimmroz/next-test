import { supabase } from "@/api/client";

export default async function getPosts(
  type: string,
  page: number,
  search: string,
) {
  if (page > 7) {
    if (type && type !== "all") {
      if (search.length > 0) {
        return supabase
          .from("posts")
          .select("*")
          .ilike("name", `%${search}%`)
          .limit(7)
          .range(page, page + 6)
          .order("date", { ascending: false });
      }
      return supabase
        .from("posts")
        .select("*")
        .like("type", type)
        .limit(7)
        .range(page, page + 6)
        .order("date", { ascending: false });
    } else {
      if (search.length > 0) {
        return supabase
          .from("posts")
          .select("*")
          .ilike("name", `%${search}%`)
          .limit(7)
          .range(page, page + 6)
          .order("date", { ascending: false });
      }
      return supabase
        .from("posts")
        .select("*")
        .limit(7)
        .range(page, page + 6)
        .order("date", { ascending: false });
    }
  }
}
